# Getting Started with Spring Boot and Docker

### Steps
* Use Spring Initializr for building a minimal project
* Add a RestController endpoint class
* build using maven
* Import in Eclipse (optional)
* Create Dockerfile


### References
* [Building an Application with Spring Boot](https://spring.io/guides/gs/spring-boot/#scratch)
* [Spring Initializr](https://start.spring.io)
* [Dockerizing a Spring Boot Application](https://stackabuse.com/dockerizing-a-spring-boot-application/)
* [Building a 'hello world' RESTful Web Service](https://spring.io/guides/gs/rest-service/)
* [Build a REST API with Spring and Java Config](https://www.baeldung.com/building-a-restful-web-service-with-spring-and-java-based-configuration)
* [Error Handling for REST with Spring](https://www.baeldung.com/exception-handling-for-rest-with-spring)


### Guides
The following guides illustrate how to use some features concretely:

* [Building a RESTful Web Service](https://spring.io/guides/gs/rest-service/)
* [Serving Web Content with Spring MVC](https://spring.io/guides/gs/serving-web-content/)
* [Building REST services with Spring](https://spring.io/guides/tutorials/bookmarks/)

