// https://howtodoinjava.com/spring-boot/spring-boot-tutorial-with-hello-world-example/
// https://www.baeldung.com/building-a-restful-web-service-with-spring-and-java-based-configuration
// changed mapping annotations based on above link

package com.example.demo;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
 
@RestController
@RequestMapping("/employees")
public class EmployeeController 
{
	//TODO: ideally this comes from backend via service autowired here
    //@Autowired
    //private EmployeeService service;
    
	List<Employee> employeesList = new ArrayList<Employee>();

	public EmployeeController() {
		employeesList.add(new Employee(1,"Lokesh","Gupta","howtodoinjava@gmail.com"));
	    employeesList.add(new Employee(2,"Alok","Gupta","howtodoinjava@gmail.com"));
		
	}
	
	@GetMapping
    public List<Employee> getEmployees() {
      return employeesList;
    }
	
	@GetMapping(value = "/{id}")
    public Employee findById(@PathVariable("id") Integer id) {
		//TODO: error handling and http response status if not found. Now it is just 500 due to NPE
		return employeesList.stream().filter(emp -> emp.getId() == id).findFirst().orElseGet(null);
    }
	
	@PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Integer create(@RequestBody Employee resource) {
		System.out.println(resource);
         employeesList.add(resource); //service.create(resource);
         return resource.getId();
    }
	
    /**
     * @param id
     * @param resource
     */
    @PutMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void update(@PathVariable( "id" ) Integer id, @RequestBody Employee resource) {
        Objects.requireNonNull(resource);
        //RestPreconditions.checkNotNull(service.getById(resource.getId()));
        //service.update(resource);
        Employee e = employeesList.stream().filter(emp -> emp.getId() == id).findFirst().orElseGet(() -> null);
        if (e != null) {
        	e.setFirstName(resource.getFirstName());
        	//TODO: other fields too
        } else {
        	System.out.println("not found");
        	throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Employee Not Found: " + id);
        }
    }
 
    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void delete(@PathVariable("id") Integer id) {
    	//TODO: refactor to remove duplication for above method and this to find employee.
        Employee e = employeesList.stream().filter(emp -> emp.getId() == id).findFirst().orElseGet(() -> null);
        if (e != null) {
        	Boolean ret = employeesList.remove(e);
        	System.out.println(ret);
        } else {
        	System.out.println("not found");
        	throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Employee Not Found: " + id);
        }
    }
}